<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Posts;
use Faker\Generator as Faker;

$factory->define(Posts::class, function (Faker $faker) {
    return [
        'slug' => $faker->sentence,
        'title' => $faker->sentence,
        'body' => $faker->paragraph,
        'completed' => 0 // Default not completed
    ];
});
