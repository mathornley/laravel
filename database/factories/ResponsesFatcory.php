<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Responses;
use Faker\Generator as Faker;

$factory->define(Responses::class, function (Faker $faker) {
    return [
        'conversation_id'=>factory(\App\Conversation::class),
        'name'=>$faker->sentence,
        'user_id'=>factory(\App\User::class)
    ];
});
