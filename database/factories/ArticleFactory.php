<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Generator as Faker;

// To create factory: php artisan make:factory ArticleFactory -m '\App\Article'
// To create articles: factory(App\Article::class,5)->create(['user_id'=>19]);


$factory->define(Article::class, function (Faker $faker) {
    return [
        'user_id' => factory(\App\User::class), // Use the user ID model
        'title' => $faker->sentence,
        'text' => $faker->paragraph,
        'excerpt' => $faker->sentence
    ];
});
