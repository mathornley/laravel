<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('names', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

         Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('number');
            $table->text('street');
            $table->string('postal_code');
            $table->timestamps();
        });

        Schema::create('address_name', function (Blueprint $table) {

            $table->primary(['name_id', 'address_id']);

            $table->unsignedBigInteger('name_id');
            $table->unsignedBigInteger('address_id');
            $table->timestamps();

            $table->foreign('name_id')->references('id')->on('names')->onDelete('cascade');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
