
@extends('layout')

@extends('bulma_styles')

@section('wrapper')
<div id="wrapper">
    <div id="page" class="container">
        <div id="content">
            <div class="title">
                <h2>Email tester</h2>
            </div>

            <form method="POST" action="/email/">
                
                @csrf {{-- Don't forget the cross site request forgery flag --}}
                
                <div class="form-group">
                    <p><label for="exampleInputEmail1">Email address</label></p>
                    <input type="text" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    <p><small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small></p>
                    <p><button name="submit" class="btn btn-primary">Submit</button></p>
                </div>
          </form>

          @error('email')
            <div class="alert alert-danger" role="alert">
                {{$message}}
            </div>
          @enderror

          @if(session('message'))
           <div class="alert alert-success" role="alert">
                {{session('message')}}
            </div>
          @endif

        </div>
    </div>
</div>
@endsection