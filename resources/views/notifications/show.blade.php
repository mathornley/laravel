@extends('layouts.app') {{-- We're using the layouts.app blade --}}

@section('styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endsection

{{-- dd($notifications) --}}

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
           <h3>The unread notifications for: {{request()->user()->name}}</h3>
           {{-- dd($notifications) --}}
           <ul>
           	@forelse($notifications['unread'] AS $notifications1)
	           	@if($notifications1->type=='App\Notifications\PaymentReceived')
	           		<li style='color:blue;'>Payment received on {{$notifications1->created_at->format('d/m/Y \a\t H:i\h\r\s')}} of {{$notifications1->data['amount']}}. Thank-you.
	           		</li>
	           	@endif
              
                  @empty
                      <p> You have no new notifications</p>
           	  @endforelse
       		</ul>

        </div>

        <div class="col-md-12">
           <h3>The read notifications for: {{request()->user()->name}}</h3>
           {{-- dd($notifications) --}}
           <ul>
            @forelse($notifications['read'] AS $notifications2)
              @if($notifications2->type=='App\Notifications\PaymentReceived')
                <li style='color:red;'>Payment received on {{$notifications2->created_at->format('d/m/Y \a\t H:i\h\r\s')}} of {{$notifications2->data['amount']}}. Thank-you.
                </li>
              @endif
              
                  @empty
                    <p> You have no new notifications</p>
              @endforelse
          </ul>

        </div>
</div>
@endsection
