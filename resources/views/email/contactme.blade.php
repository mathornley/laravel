<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>The HTML5 Herald</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="css/styles.css?v=1.0">

</head>

<body>
  <h1>It works {{$name}}!</h1>

  <p> It sounds like you want to hear more about {{$string}}</p>
  <p> An article selected is: {{$title}}</p>


  <button>Click this </button>

</body>
</html>