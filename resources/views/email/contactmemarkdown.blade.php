<!-- DO NOT indent the code as it won't render -->

@component('mail::message')

## This is the heading

A list goes here

- This
- Is
- a
- list

<!-- This is simply loading a laravel view within a vendor dependancy: mail::button
laravel\vendor\mail/respources\views\html\button.blade.php
-->
@component('mail::button', ['url'=> 'http://mikedeveloper.com'])
	Visit Mikedeveloper.com
@endcomponent

@endcomponent