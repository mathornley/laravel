{{-- ddd(Auth::user()->articles) --}}

@extends('layouts.app') {{-- We're using the layouts.app blade --}}

@section('styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<style>
	button{
    padding:0px;
    background-color: #fff;
    color:#000;
    border:0;
    outline:0px;
}

.alert {
    position: relative;
    padding: 10px 5px;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: .25rem;
}
</style>
@endsection

@section('content')
	<div class="container">
		<p><a href='/conversations/'>Go back to conversations</a></p>
		<div class="row justify-content-center">
			<div class="col-md-12">
				<h3>{{$conversation->title}}</h3>
				<p><small>Posted by: {{$conversation->user->name}}</small></p>
				<p>{{-- $conversation->user->name --}}</p>
				<hr/>
				<p>{{$conversation->body}}</p>
			</div>
		</div>

		<h4>Replies</h4>
		<hr/>
		<div class="row justify-content-center">
			<div class="col-md-12">
				
					@forelse($conversation->responses AS $responses)
						
						@if($responses->isBtestReply())
							<div style='display:flex;justify-content:space-between;'>
								<strong>{{$responses->user->name}}</strong> says..... 
								<span style='color:green;font-weight: bold;'>best reply!!</span>
							</div>
						@else
							<strong>{{$responses->user->name}}</strong> says.....
						@endif

						<p>{{$responses->name}}</p>
							
						{{-- Moved from Auth service provider boot method --}}	
						
						{{-- Can this user choose the best reply - See the ** Auth service provider ** or the conversation policy --}}
						
						@can("update-conversation",$conversation)
							
							<form method="POST" action='/conversations/{{$responses->id}}/'>
							
								@csrf
								
								<div class="form-row">
								    <div class="col">
				   				      	<button type="submit">Best reply?</button>
								    </div>
								</div>
							</form>
						
						@endcan

						<hr/>

						@empty
							No comments as of yet
					@endforelse
		</div>
</div>
@endsection
