@extends('layout')

@section('bulma_styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
@endsection

{{--print_r($errors)--}}

@section('wrapper')

<div id="wrapper">
    <div id="page" class="container">

        <h2 class='has-text-weight-bold is-size-4'> New Article</h2>
        <br/>
        <form method='POST' action="{{route('articles.index')}}"> {{-- METHOD IS POST --}} 
            @csrf {{-- Don't forget the cross site request forgery flag --}}
            <div class="field">
                <label class="label">Title</label>
                <div class="control">
                    <input 
                    id='title' 
                    class="input @error('title') is-danger @enderror" 
                    type="text" 
                    name='title' 
                    placeholder="Title here"
                    value="{{old('title')}}">
                    
                    @error('title')
                        <p class='help-is-danger'>{{$errors->first('title')}}</p>
                    @enderror
                
                </div>
            </div>

            <div class="field">
                <label class="label">Text</label>
                <div class="control">
                    <textarea
                        id='text' 
                        class="textarea {{$errors->first('text') ? 'is-danger' : ''}}" 
                        name='text' 
                        placeholder="Text here"
                        value="{{old('text')}}">{{old('text')}}</textarea>

                    @if($errors->has('text'))
                        <p class='help-is-danger'>{{$errors->first('text')}}</p>
                    @endif

                </div>
            </div>

            <div class="field">
                <label class="label">Exerpt</label>
                <div class="control">
                    <input id='excerpt' 
                    class="input {{$errors->first('excerpt') ? 'is-danger' : ''}}" 
                    type="text" 
                    name='excerpt' 
                    placeholder="Excerpt here"
                    value="{{old('excerpt')}}">
                    
                    @if($errors->has('excerpt'))
                        <p class='help-is-danger'>{{$errors->first('excerpt')}}</p>
                    @endif

                </div>
            </div>

             <div class="field">
                <label class="label">Tags</label>
                <div class="control select is-multiple">
                    
                    <select name="tags[]" 
                    multiple
                    >

                           @foreach($tags AS $tag)
                                <option value="{{$tag->id}}">{{$tag->name}}</option>
                           @endforeach

                    </select>

                   {{-- This is the alternative syntax for the display of errors--}}
                   @error('tags')
                        {{--<p>{{ $errors->first('tags')}}</p>--}} {{--You can use either syntax --}}
                        <p>{{ $message }}</p>
                   @enderror

                </div>
            </div>


            <div class="field">
                <input class="button" type="submit" value="Submit">
            </div>
        </form>
    </div>
</div>

@endsection