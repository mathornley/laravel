@extends('layout')

@section('wrapper')
<div id="wrapper">
    <div id="page" class="container">
        <div id="content">
            <div class="title">
                <h2>Welcome to our Articles page</h2>
                <div id="wrapper">

                    @if(isset($articles->message)) {{--If there is a message, show it--}}
                        <p>These articles are tagged in: <strong>{{$articles->message}}</strong></p>
                    @endif
                    
                    <ul class="style1">
                        <ul>
                            @forelse($articles AS $article) {{-- Foreach with if and else (else is empty)--}}

                                {{--Ordinarily the post would not be hardcoded but this is just an example--}}  
                                <h3>{{$article->title}}</h3>

                                @can('update-article',$article)
                                <h3><a href="{{$article->edit()}}">Edit this article</a></h3>
                                @endcan

                                <p>Created at: <strong>{{\Carbon\Carbon::parse($article->created_at)->format('d/m/Y H:i\h\r\s')}}</strong></p>
                                <p><img src="/images/banner.jpg" alt="" class="image image-full" /> </p>
                                <p>{{$article->text}}</p>
                                <p>{{$article->excerpt}}</p>

                                {{-- No articles to be found --}}
                                @empty
                                <p>No articles just yet.</p>

                            @endforelse
                        </ul>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
