@extends('layout')

@section('wrapper')
<div id="wrapper">
    <div id="page" class="container">
        <div id="content">
            <div class="title">
                <h2><a href="{{route('articles.edit',$article)}}">{{$article->title}} {{--Request::path()--}}</a></h2>
            </div>
            <p><img src="/images/banner.jpg" alt="" class="image image-full" /> </p>
            <p>{{$article->text}}</p>
            <p>{{$article->excerpt}}</p>
            <p>
        		@foreach($article->tag AS $tags)

        			{{--<a href="/articles?tag={{$tags->name}}">{{$tags->name}}</a> --}}
                    <a title='All posts on {{$tags->name}}' href="{{route('articles.index', ['tag'=>$tags->name])}}">{{$tags->name}}</a>
                @endforeach

    		</p>
            <p>Written by <strong>{{$article->user->name}}</strong></p>
        </div>
    </div>
</div>
@endsection