@extends('layout')

@section('bulma_styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
@endsection

@section('wrapper')

<div id="wrapper">
    <div id="page" class="container">

        <h2 class='has-text-weight-bold is-size-4'> Edit Article</h2>
        <br/>
        <form method='POST' action="{{route('articles.update', $article)}}"> {{-- METHOD IS POST --}} 
            
            @csrf {{-- METHOD TO STOP CROSS SITE SCRIPTING --}}
            @method('PUT') {{-- METHOD IS NOW PUT but modern browsers won't accept PUT request, this would be the same for DELETE and PATCH as well --}} 

            <div class="field">
                <label class="label">Title</label>
                <div class="control">
                    <input class="input" type="text" name='title' placeholder="Title here" value='{{$article->title}}'>
                    @error('title')
                        <p class='help-is-danger'>{{$errors->first('title')}}</p>
                    @enderror
                </div>
            </div>

            <div class="field">
                <label class="label">Text</label>
                <div class="control">
                    <textarea class="textarea" name='text' placeholder="Text here">{{$article->text}}</textarea>
                    @error('text')
                        <p class='help-is-danger'>{{$errors->first('text')}}</p>
                    @enderror
                </div>
            </div>

            <div class="field">
                <label class="label">Exerpt</label>
                <div class="control">
                    <input class="input" type="text" name='excerpt' placeholder="Excerpt here" value='{{$article->excerpt}}'>
                    @error('excerpt')
                        <p class='help-is-danger'>{{$errors->first('excerpt')}}</p>
                    @enderror
                </div>
            </div>

            <div class="field">
                <input class="button" type="submit" value="Submit">
            </div>
        </form>

        <!-- Just added an if statement to show if the record has been
        @if($article->updated_at)
            <!--<p>The article was updated on: {{$article->updated_at}} </p>-->
            <p>The article was updated on: {{\Carbon\Carbon::parse($article->updated_at)->format('d/m/Y H:i\h\r\s')}}</p>
        @endif

    </div>
</div>

@endsection