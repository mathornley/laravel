
<h2> My Site: test permissions</h2>

{{auth()->user()->name}}


<ul>
	
	@can('edit_forum')
		<li><a href='#'>Edit forum</a></li>
	@endcan
	
	@can('view_reports')
		<li><a href='#'>View reports</a></li>
	@endcan
</ul>