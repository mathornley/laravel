<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />
<link href="/default.css" rel="stylesheet" type="text/css" media="all" />
<link href="fonts.css" rel="stylesheet" type="text/css" media="all" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

@yield('bulma_styles')

</head>
<body>
<div id="header-wrapper">
   
    <div id="header" class="container">
        <div id="logo">
            <h1><a href="/">Mike Laravel test</a></h1>
        </div>
        <div id="menu">
            <ul>
                {{-- Either the Request::path() or Fequest::is(<REGEXP>)--}}
                <li class="{{Request::is('/') ? 'current_page_item' : ''}}"><a href="/" accesskey="1" title=" The homepage">Homepage</a></li>
                <li class="{{Request::path() == 'clients' ? 'current_page_item' : ''}}"><a href="/clients" accesskey="1" title="Our Clients">Our Clients</a></li>
                <li class="{{Request::path() === 'about' ? 'current_page_item' : ''}}"><a href="/about" accesskey="1" title="About us">About Us</a></li>
                <li class="{{Request::path() == 'articles' ? 'current_page_item' : ''}}"><a href="/articles" accesskey="1" title="Articles">Articles</a></li>
                <li class="{{Request::path() == 'contact' ? 'current_page_item' : ''}}"><a href="/contact" accesskey="1" title="Contact Us">Contact Us</a></li>
            </ul>
        </div>
    </div>

@yield('banner')

</div>

    
@yield('wrapper')

<div id="copyright" class="container">
    <p>&copy; Mikedeveloper {{ date('Y') }} All rights reserved</p>
</div>
</body>
</html>
