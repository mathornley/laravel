-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2020 at 04:11 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `abilities`
--

CREATE TABLE `abilities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abilities`
--

INSERT INTO `abilities` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'read_reports', NULL, NULL, NULL),
(2, 'view_reports', NULL, NULL, NULL),
(3, 'edit_forum', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ability_role`
--

CREATE TABLE `ability_role` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `ability_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ability_role`
--

INSERT INTO `ability_role` (`role_id`, `ability_id`, `created_at`, `updated_at`) VALUES
(1, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `number` bigint(20) UNSIGNED NOT NULL,
  `street` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `number`, `street`, `postal_code`, `created_at`, `updated_at`, `phone_number`) VALUES
(1, 7, 'Mode Hill walk', 'M458JZ', NULL, NULL, NULL),
(2, 2, 'Selby Avnue', 'M45 8UT', NULL, NULL, NULL),
(3, 112, 'king Street', 'BB1 6GT', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `address_name`
--

CREATE TABLE `address_name` (
  `name_id` bigint(20) UNSIGNED NOT NULL,
  `address_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `address_name`
--

INSERT INTO `address_name` (`name_id`, `address_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `user_id`, `title`, `text`, `excerpt`, `created_at`, `updated_at`) VALUES
(1, 106, 'Repellat velit aperiam eveniet eos nemo et animi deserunt.', 'Quas facere accusamus iure deleniti et sed. Ipsa molestiae ut autem ratione eum officia.', 'Quasi quis magni odio et quae in doloribus.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(2, 7, 'Consequatur cupiditate dolorem recusandae facilis explicabo.', 'Perspiciatis dolor aspernatur consequatur facere nobis ut nulla. Et quas autem dolores sit tempora. Repellat expedita qui beatae ducimus voluptatem laudantium.', 'Placeat eaque corporis quos quisquam enim eligendi quidem labore.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(3, 8, 'Nulla consequatur expedita quis.', 'Natus a fugiat qui et molestias. Dolorem quasi inventore qui ratione quia autem. Et aut ipsum dicta delectus sint.', 'Repudiandae laudantium voluptatem sunt sapiente assumenda quibusdam sit.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(4, 9, 'Porro sint voluptas quibusdam et sed fugiat ut.', 'Ut officia eum asperiores. Est ab expedita eligendi voluptatum ipsum. Eaque et et quasi voluptas eius ducimus asperiores.', 'Voluptatibus facilis tempora qui totam sint excepturi dolore.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(5, 10, 'Assumenda recusandae eum non porro ducimus est delectus.', 'Eveniet qui veritatis ut est id quod. Nesciunt hic commodi maxime omnis quasi. Qui consequatur mollitia sequi incidunt. Enim consequatur deserunt consequatur et ipsum.', 'Omnis nam libero labore qui.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(6, 11, 'Et ratione voluptatem commodi maiores non illo molestiae.', 'Quis a dolor sapiente praesentium impedit eos. Consequatur fuga temporibus sapiente inventore voluptatem aut. Sed molestias omnis dolor doloremque porro.', 'Tempora omnis doloribus fugit unde repellat.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(7, 12, 'Repellendus ipsum non perspiciatis ut exercitationem facere soluta qui.', 'Totam corrupti voluptas velit quasi fuga ut. Nihil ut minima omnis quia unde. Blanditiis dicta eum dolor recusandae sapiente quis vel.', 'Eos incidunt voluptate ut molestiae libero ducimus.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(8, 13, 'Vitae harum non odit voluptates.', 'Omnis voluptate ut ut vero officiis qui. In ut autem inventore numquam optio harum. Ipsum occaecati commodi id quia qui repellendus. Inventore veniam autem unde nobis ea quia minima. Ducimus natus ab sed quia eos aut explicabo.', 'Molestias in dolorem saepe debitis ratione qui.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(9, 14, 'Et molestias aliquid quod velit consectetur fugit aut asperiores.', 'Sunt rem amet itaque error vero. Et pariatur facilis excepturi est et rerum. Odit pariatur sed aut est et. Ut dolor dolor quis quia.', 'Rerum doloremque iste veritatis labore aut quia non.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(10, 15, 'Veritatis et repellat animi repudiandae id.', 'Maxime aliquam unde veritatis sed consequatur. Perferendis non nobis totam est inventore similique. Et amet eaque dolorem non. Eum et porro velit quos et necessitatibus. Ex eos in blanditiis at cupiditate.', 'Quos quia repellat a ex consequatur iure.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(11, 16, 'Dolor et et at.', 'Quisquam ex est enim sint. Cum rerum ut tempora ipsam autem ut cum. Est corporis occaecati ab.', 'Unde voluptas dolor culpa voluptatem beatae ipsum mollitia.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(12, 17, 'Quod eos est nulla amet deserunt molestiae doloribus.', 'Sequi suscipit et odio quidem eligendi magni et. Reiciendis et voluptas qui est. Perspiciatis neque quidem porro dignissimos et. Accusantium est minus dolorem quasi eum consequatur.', 'Est unde alias iusto aut culpa doloribus.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(13, 18, 'Minus nihil expedita ea mollitia accusamus et vero.', 'Sed at maiores velit in ea. Quia assumenda magnam et quisquam rerum ut. Aut exercitationem aut cum vel aut. Quibusdam voluptatem quis dolore deserunt rerum officiis.', 'Quasi ut accusamus et dolores eligendi.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(14, 19, 'Eius qui dignissimos ea itaque voluptatem.', 'Voluptatem voluptatibus doloribus exercitationem aut. Laborum necessitatibus est itaque quibusdam minus delectus sint. Eos reiciendis dignissimos impedit. Delectus et quia aut delectus cumque.', 'Soluta occaecati reiciendis cupiditate possimus.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(15, 20, 'Soluta voluptas repellendus tenetur ipsum vitae qui.', 'Doloremque et enim odio doloremque illo recusandae. Similique sit dignissimos hic ut ea provident voluptatum natus. Hic voluptatem explicabo incidunt facere officia. Cumque ducimus aut occaecati maiores.', 'Quia id perferendis deserunt est dolor.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(16, 21, 'Voluptatem quidem ratione ipsum dolorum laboriosam voluptas aut.', 'Sint minus consequatur sapiente voluptas consectetur ea corrupti. Quia autem explicabo est fugit facere ab. Dolorem beatae nam omnis illum. Maiores deserunt sed vero omnis molestiae at quis sapiente.', 'Neque labore ut ad accusantium voluptates ab velit minima.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(17, 22, 'Aperiam rerum possimus voluptas quae.', 'Nobis est dolores fuga accusantium qui illum. Rerum cumque et nostrum aspernatur. Atque ut exercitationem commodi aut quo quaerat eligendi beatae.', 'Quaerat rerum et qui sed.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(18, 23, 'Placeat qui et maiores.', 'Omnis officiis quidem a quo ea culpa et id. Sit inventore optio est id totam dolorem corporis. Qui reiciendis minus nulla. Ut quia repellendus voluptas inventore labore.', 'Nulla sed fuga omnis omnis officiis.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(19, 24, 'Esse praesentium ullam iste dolorum explicabo porro quia.', 'Quam iusto tempore unde molestias. Odio dolore voluptates placeat id reiciendis sit et. Deleniti soluta quia ad natus.', 'Aut sequi est molestias incidunt sit ipsam non.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(20, 25, 'Quam suscipit voluptas rerum nisi.', 'Perspiciatis nihil dolores provident omnis. Molestias et adipisci est enim. Porro eum et atque iusto. Ea omnis id blanditiis quod vero.', 'Et non soluta voluptatibus voluptatem voluptas.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(21, 26, 'Eum iure nemo illo dolores unde error.', 'Aut quas provident minus et iusto. Cum ut eveniet ipsa. Nobis officia adipisci facilis reiciendis assumenda recusandae neque. Dolore non laudantium sint laborum nobis in.', 'Doloribus nobis consequatur neque rerum asperiores.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(22, 27, 'Earum voluptatem tempore libero ut iusto sed.', 'Vero vel sequi aut qui magni nobis. Magnam voluptatem quidem sit exercitationem. Sint sed occaecati eum nobis.', 'Quae dolorum iusto est aliquid quia culpa odit.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(23, 28, 'Est odit tempora ducimus minima.', 'Sed nihil ipsam et rem adipisci. Veritatis consequatur vero ducimus. Quidem explicabo voluptatibus consequuntur. Nemo officia doloribus voluptas nulla voluptate.', 'Eaque omnis et cupiditate aspernatur modi illum.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(24, 29, 'Illo placeat praesentium cumque non sit in.', 'Molestias debitis molestiae alias provident. Est et at aperiam perspiciatis unde.', 'Eum labore et ipsa ab ea ipsam.', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(25, 30, 'Iure temporibus facilis dolorem.', 'Expedita dolor magnam error. Quibusdam aut voluptatem id enim provident repellendus ut. Sapiente est voluptas eos voluptatibus saepe autem. Nobis dolores occaecati autem itaque repellendus non.', 'Necessitatibus culpa et vel eum nulla necessitatibus eveniet.', '2020-04-22 14:57:46', '2020-04-22 14:57:46');

-- --------------------------------------------------------

--
-- Table structure for table `article_tag`
--

CREATE TABLE `article_tag` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `article_id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `article_tag`
--

INSERT INTO `article_tag` (`id`, `article_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(1, 1, 4, NULL, NULL),
(2, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `best_reply_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `conversations`
--

INSERT INTO `conversations` (`id`, `user_id`, `title`, `body`, `best_reply_id`, `created_at`, `updated_at`) VALUES
(1, 106, 'Neque expedita doloremque recusandae quia molestias.', 'Porro tenetur hic qui esse mollitia. Quia quis excepturi quis quia commodi repellat accusantium. Dolores sequi nesciunt cum rerum et tenetur. Harum tenetur labore eligendi.', 3, '2020-04-22 14:57:58', '2020-04-23 09:08:33'),
(2, 32, 'Voluptatem suscipit molestiae et deleniti aperiam omnis qui.', 'Natus eveniet cupiditate deleniti ipsa. Quam ut reprehenderit voluptas sint beatae libero rerum voluptas.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(3, 33, 'Omnis quibusdam qui quisquam quia animi.', 'Aut sit et iure amet accusantium incidunt est. Velit sed consequuntur voluptatem vel beatae aliquam molestiae. Fuga qui impedit asperiores voluptate officiis enim. Est mollitia pariatur in et consequatur vero.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(4, 34, 'Ipsa enim architecto a pariatur nostrum aut.', 'Aut rem repellat temporibus numquam et rerum sit veniam. Quo voluptatem nam non quidem rerum odio praesentium molestias. Voluptatem beatae sed dolore necessitatibus excepturi laborum. Suscipit consequatur nihil autem iure facere velit consectetur.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(5, 35, 'Facere sit qui ut officiis dignissimos.', 'Exercitationem veritatis laudantium soluta enim ut repudiandae. Perspiciatis et vero consequatur qui dolor aspernatur et. Officiis consectetur voluptate tempora voluptatibus harum praesentium. Eos commodi quidem facilis dicta.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(6, 36, 'Consequuntur rem et fugit eveniet.', 'Aut perferendis ipsam non. Rem blanditiis ut eum deleniti. Laborum aut id qui error non fuga. Delectus laborum architecto sed sit aut voluptas.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(7, 37, 'Numquam modi enim voluptatem qui sed ea iste.', 'Ipsum dolores saepe iste et adipisci. Quibusdam quia quia dolorem voluptas maiores cumque non. Esse quo dolorem earum perspiciatis reprehenderit similique.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(8, 38, 'At qui repellat nam.', 'Aspernatur eaque et et sapiente aut et excepturi. Sed aliquid distinctio facere sunt quo vel. Voluptatem sint enim aspernatur. Optio ratione dolorum minima voluptatem inventore architecto autem doloremque.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(9, 39, 'Enim unde quasi ut consequatur iure magnam.', 'Officia repellat iure quasi enim dolorem. Ab harum velit et et odit aut. Ea ipsa non nemo ratione voluptates.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(10, 40, 'Quia facere veritatis odio nihil ut nulla dolores aut.', 'Ex suscipit ut ad ipsam amet a dolorem. Natus ducimus officia recusandae accusantium velit sequi ad. Quos id officiis explicabo consectetur necessitatibus provident.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(11, 41, 'Sed beatae asperiores omnis repellat totam repudiandae.', 'Voluptas et recusandae ex sint. Nisi fuga dolores atque voluptatem sed at vitae. Perferendis corrupti rerum omnis occaecati velit qui.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(12, 42, 'Corrupti pariatur quae et.', 'Natus eos deserunt dolores. Quia iste ea nemo saepe expedita. Voluptates est accusantium corporis aut nostrum consequatur. Magni iste itaque odio est.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(13, 43, 'Laboriosam libero eos laborum soluta cumque sed necessitatibus amet.', 'Excepturi id ut ea sed esse culpa. Aperiam officia voluptate esse et voluptas. Aliquam eveniet non doloribus dolorem eum vitae maiores non. Natus aspernatur occaecati ex quo quaerat exercitationem.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(14, 44, 'Dolores ipsa eveniet sit natus quia.', 'Officia dolore molestiae aut inventore. Voluptas voluptas dignissimos voluptate impedit nemo debitis quis. Possimus culpa sed autem qui illum aliquam. Doloremque quam ut exercitationem rerum enim sed libero. Vero earum quo vero qui tempora.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(15, 45, 'Qui sit illum itaque.', 'Voluptatem veniam nihil quam fuga. Labore doloribus amet explicabo sunt est fugit aut quibusdam. Voluptas omnis architecto debitis perspiciatis.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(16, 46, 'Enim sint architecto rem est unde dolorem dolorem.', 'Laudantium delectus officia ad dolor ut magni sit. Quo omnis deleniti rerum nemo aut laboriosam veritatis. Excepturi sit veritatis suscipit explicabo voluptatem culpa vero quasi. Numquam saepe quos omnis dolorem nostrum.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(17, 47, 'Aliquid praesentium culpa autem sint.', 'Est mollitia deleniti maxime animi et ut neque. Ipsam vel molestias voluptas pariatur.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(18, 48, 'Sed blanditiis libero impedit perferendis quasi sit in.', 'Corporis qui tenetur consequuntur nulla. Odio sint aperiam sapiente quo dicta. Eius nulla repudiandae similique debitis officia eveniet. Et praesentium ullam doloribus pariatur.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(19, 49, 'Architecto pariatur et sit sed est alias.', 'Vitae ullam et aut. Porro omnis culpa voluptas quam neque sapiente similique. Ut ut minus odit repellendus voluptas qui ex.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(20, 50, 'Vitae in earum quam inventore.', 'Architecto numquam quidem non sit sunt. Necessitatibus unde praesentium sed pariatur autem cum molestiae quia. Itaque quod sint natus aut consequatur.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(21, 51, 'Porro occaecati suscipit voluptas laboriosam.', 'Recusandae aut ea omnis quam laboriosam aut eum. Voluptatem ipsa eos et voluptas soluta reprehenderit. Ut rerum corporis qui expedita.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(22, 52, 'Libero aperiam aut molestias et commodi voluptate qui.', 'Nostrum pariatur hic quas totam a voluptas voluptatem. Occaecati expedita eius ipsam rem et deleniti quo nihil. Delectus ex nisi expedita architecto et laudantium. Est beatae doloremque amet in rerum sit.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(23, 53, 'Aut ea aliquam fugit porro.', 'Quos in sapiente at asperiores ex. Aut provident velit voluptatem et. Sint quibusdam et ut aut rerum aut vel. Hic officia qui non laboriosam aspernatur nam ipsum. Natus est exercitationem sed harum.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(24, 54, 'Sapiente asperiores esse nesciunt itaque.', 'Consequatur veniam magni eius quibusdam recusandae nemo. Accusantium non aut ut id. Repellendus quae sapiente quis est.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(25, 55, 'Eos ut ipsam nemo officia illum.', 'Ad laborum sequi rerum fugiat et sint. Aut iure dolor magni recusandae. Omnis doloribus non quos excepturi quaerat laboriosam. Possimus ab quis nemo esse fugit impedit mollitia.', NULL, '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(26, 56, 'Rerum similique voluptas magnam qui voluptatem.', 'Aspernatur odit minus reiciendis repudiandae. Et enim necessitatibus eaque. Ipsam fugit atque nostrum.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(27, 58, 'Minus iste ea facilis enim debitis.', 'Eveniet alias ea autem numquam quos quia. Voluptatum debitis eveniet maxime aut vel eligendi suscipit. Officia occaecati ut tempore minus et est repudiandae qui. Saepe odio magni nihil voluptatem suscipit.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(28, 60, 'Recusandae nisi dicta amet id ullam rem velit.', 'Dolorem est quas dolor magni sapiente aperiam. Et et nihil et praesentium ipsam exercitationem rerum. Dolorem dolores sapiente non beatae. Aspernatur maxime et voluptatem rem accusantium aliquid.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(29, 62, 'Laudantium id et aliquam optio ex.', 'Ea quod consequuntur id. Nihil sunt officiis aliquam. Saepe commodi fugit perspiciatis molestiae ut. Id sit iste molestiae perferendis excepturi quia et. Quia laboriosam molestias quis exercitationem repudiandae amet occaecati.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(30, 64, 'Tenetur aut tenetur nihil dignissimos rem.', 'Dolorem fuga hic molestiae qui consequatur adipisci molestiae. Dolorem culpa cupiditate rerum adipisci consequuntur perferendis nisi. Quo facere rerum nemo vel mollitia impedit consequatur.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(31, 66, 'Et sint magni sequi nobis non.', 'Qui cumque eius non odio cum impedit. Dolores qui dolores a quis. Doloribus consectetur et qui tempore laborum. Neque enim veniam est quidem sed dignissimos.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(32, 68, 'Ut laborum et nisi est enim rem ad optio.', 'Aspernatur optio explicabo iusto debitis voluptatem nesciunt quae. Consequatur quia aut quisquam rerum dolores delectus. Quam tempore optio distinctio eos harum totam repellendus. Omnis sapiente fugiat voluptatem ipsa.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(33, 70, 'Deleniti aliquam possimus optio qui molestiae ipsa doloremque non.', 'Et deleniti ab occaecati eos nam. Eum et qui omnis et. Natus id voluptates sint blanditiis dolorem et illo consequatur.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(34, 72, 'Saepe asperiores ducimus quo a ipsa.', 'Neque debitis suscipit suscipit quia vitae. Voluptatem ipsum ut sequi qui. Facere ea inventore aliquid dolore impedit provident.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(35, 74, 'Est qui sunt maxime quisquam.', 'Ex ab itaque ea officiis sit provident atque. Quo molestiae culpa assumenda quia illum quam. Amet possimus voluptatem recusandae libero.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(36, 76, 'Ex voluptatem assumenda eum nam porro beatae.', 'Minima optio exercitationem non qui incidunt fugiat officiis. Laboriosam commodi ut expedita a reprehenderit. Sit tenetur voluptatem repudiandae qui recusandae ullam eos. Perspiciatis quia qui saepe accusamus.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(37, 78, 'Ipsam autem suscipit aut facilis.', 'Exercitationem eligendi excepturi consequatur maxime provident rem quidem. Aut vel et enim libero ducimus tempore. Officia earum sed cum beatae unde et nostrum. Tenetur et laudantium dolor praesentium. Delectus voluptatem harum fugiat qui eum optio doloremque.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(38, 80, 'Voluptatem rem ex asperiores amet eius illum ut.', 'Eos non omnis hic voluptatem. Eius praesentium delectus eaque rerum. Voluptatem sint aliquam tenetur.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(39, 82, 'Et id cum veritatis cum.', 'At repudiandae aliquid aut est consequatur itaque error. Et ut consequatur aut nesciunt velit quia. Error ab est consequuntur assumenda.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(40, 84, 'Qui voluptatum aperiam voluptates adipisci aspernatur maxime.', 'Soluta iusto sit illo reiciendis voluptatem. Ut minima aut assumenda excepturi sit. Soluta sapiente incidunt explicabo ipsam aut corporis.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(41, 86, 'Ut amet qui quas.', 'Reiciendis ipsa nostrum aperiam expedita similique ducimus tempore alias. Eaque error recusandae sint quis officiis odit rem. Sint ea magni nemo magni ratione.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(42, 88, 'Est quia ipsam deleniti inventore.', 'Architecto qui quas pariatur qui sequi ullam odio. Velit qui perspiciatis minus quis incidunt fugiat. Provident earum voluptatem facere nulla reiciendis.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(43, 90, 'Esse consequatur eum aperiam soluta aut est tenetur.', 'Ut ipsa eum accusantium quam voluptas atque molestias. Quod aut quibusdam aut quia sint non culpa qui. Amet quos voluptatem eos doloribus sit.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(44, 92, 'Reprehenderit tempora in quia cumque voluptas consequatur voluptatem odit.', 'Delectus cum rerum ut numquam est quod. Expedita dolor expedita velit placeat architecto sit exercitationem dolore. Dolorem et quas sed quasi recusandae laudantium dolor.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(45, 94, 'Et autem numquam voluptas est temporibus quae.', 'Esse labore repellat enim id. Sed ea saepe ab dolor doloribus magni deserunt. Alias laborum voluptas et. Ducimus dolor nam aperiam quis error saepe ut.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(46, 96, 'Et veniam ut quas nobis quis rerum et.', 'Vitae porro quaerat est eum culpa. Nisi quibusdam nulla sit. Nesciunt fugiat aut magni numquam nulla consectetur suscipit. Quod rerum corporis eos et consectetur. Est quia suscipit excepturi iste quaerat fugit.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(47, 98, 'Vel non nemo saepe ut facilis.', 'Quaerat iure et quidem impedit voluptatem perferendis ut. Nihil voluptates ut commodi sunt ipsum fugiat fugit.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(48, 100, 'Aspernatur tempore soluta ea sed suscipit veniam ducimus.', 'Saepe provident magnam voluptatem ea est. Aut tempore laboriosam ut rerum necessitatibus expedita vero. In velit doloribus rerum id maxime fugit. Perspiciatis explicabo nemo laboriosam in fuga impedit adipisci.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(49, 102, 'Et assumenda beatae natus sed quae omnis rem.', 'Minima labore facilis quod provident sed molestiae tempora. Quis sit dolores laborum facere totam. Ipsum voluptates odit repudiandae et dignissimos sed. Incidunt consectetur error sed eveniet atque. Rerum eaque omnis quia quas accusamus modi.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(50, 104, 'Aut ex voluptatem quo eveniet voluptas enim iusto.', 'Ut non reprehenderit et tempora aut rerum laboriosam. Non aut ut adipisci dolor in totam magnam iure. Qui aspernatur quod voluptates laboriosam fugiat deserunt corporis quia.', NULL, '2020-04-22 14:58:12', '2020-04-22 14:58:12');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_01_16_180234_create_posts_table', 1),
(5, '2020_01_16_180840_add_title_to_posts_table', 1),
(6, '2020_03_26_124646_create_notifications_table', 1),
(7, '2020_04_03_104501_create_conversations_table', 1),
(8, '2020_04_07_093030_create_articles_table', 1),
(9, '2020_04_09_123838_create_tags_table', 1),
(10, '2020_04_09_154342_create_responses_table', 1),
(11, '2020_04_22_090349_create_roles_table', 1),
(12, '2020_04_22_125308_create_names_table', 1),
(13, '2020_04_22_131514_add_phone_to_addresses_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `names`
--

CREATE TABLE `names` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `names`
--

INSERT INTO `names` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Mike Thornley', NULL, NULL),
(2, 'Ann Hakin', NULL, NULL),
(3, 'tester', '2020-04-22 12:45:24', '2020-04-22 12:45:24'),
(4, 'This is a tester', '2020-04-22 12:54:36', '2020-04-22 12:54:36'),
(5, 'Cordie Medhurst', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(6, 'Max Trantow V', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(7, 'Ole Ernser', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(8, 'Mrs. Alysson Witting PhD', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(9, 'Afton Grimes MD', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(10, 'Imelda Robel', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(11, 'Mrs. Priscilla White DVM', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(12, 'Jorge Kihn PhD', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(13, 'Georgiana Watsica', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(14, 'Nellie Parisian', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(15, 'Ms. Nella Thompson DDS', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(16, 'Esteban Rohan', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(17, 'Mrs. Ova Lindgren', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(18, 'Pearlie Bruen', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(19, 'Lavonne Roob', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(20, 'Mr. Triston Berge IV', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(21, 'Prof. Hugh Hills III', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(22, 'Dr. Sigrid Russel Jr.', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(23, 'Dr. Jewell Runolfsdottir DVM', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(24, 'Cordell Swift', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(25, 'Moshe Beahan', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(26, 'Noemy Witting', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(27, 'Prof. Cade Hessel', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(28, 'Elmo Treutel', '2020-04-22 15:01:00', '2020-04-22 15:01:00'),
(29, 'Imelda Gutmann', '2020-04-22 15:01:00', '2020-04-22 15:01:00');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('a70687d7-438c-4581-b709-a3a5beb7b0d9', 'App\\Notifications\\PaymentReceived', 'App\\User', 106, '{\"amount\":900,\"type\":\"test\"}', NULL, '2020-04-27 12:20:43', '2020-04-27 12:20:43');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  `published_on` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `responses`
--

CREATE TABLE `responses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `conversation_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `responses`
--

INSERT INTO `responses` (`id`, `conversation_id`, `user_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 106, 57, 'Reiciendis velit corrupti accusamus qui qui ipsam dolore.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(2, 106, 59, 'Necessitatibus voluptate atque consequuntur delectus.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(3, 1, 1, 'Et soluta eveniet odit voluptas.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(4, 1, 65, 'Quia voluptatem sed rerum perspiciatis ex maxime ut autem.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(5, 2, 65, 'Accusamus aut est vel quaerat ex inventore.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(6, 2, 67, 'Odio deleniti tenetur dolores earum rerum.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(7, 32, 69, 'Nihil quidem rerum optio veritatis asperiores expedita eos ipsum.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(8, 33, 71, 'Unde eligendi assumenda eius totam natus sit accusantium.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(9, 34, 73, 'Inventore consequatur molestiae sit pariatur omnis sit.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(10, 35, 75, 'Totam qui illo ut dolores.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(11, 36, 77, 'Quidem quasi architecto reiciendis consequatur sed omnis voluptate excepturi.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(12, 37, 79, 'Quibusdam sint ut consequatur pariatur corporis dignissimos quod.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(13, 38, 81, 'Aut in aut cumque quis amet.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(14, 39, 83, 'Vero aspernatur recusandae explicabo libero quos aut.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(15, 40, 85, 'Et quis atque ipsam ut.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(16, 41, 87, 'Ut qui quidem dolorem.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(17, 42, 89, 'Nulla non omnis iure earum dolore id.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(18, 43, 91, 'Dicta accusamus aut quod magni recusandae doloremque vel qui.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(19, 44, 93, 'Minus non delectus quia veritatis.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(20, 45, 95, 'Nihil et voluptate quos aut.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(21, 46, 97, 'Nesciunt nostrum pariatur odio delectus autem.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(22, 47, 99, 'Voluptatibus sunt hic quos.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(23, 48, 101, 'Et earum eos quisquam facilis debitis explicabo.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(24, 49, 103, 'Debitis consequuntur odio voluptatem et voluptatibus accusantium.', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(25, 50, 105, 'Laudantium delectus voluptate et voluptatem temporibus nesciunt dolores.', '2020-04-22 14:58:12', '2020-04-22 14:58:12');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'Moderator', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 106, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'PHP', NULL, NULL),
(2, 'Work', NULL, NULL),
(3, 'Education', NULL, NULL),
(4, 'Laravel', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Katharina Cronin', 'bernadine90@example.com', '2020-04-22 14:57:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SEWNDzWoRY', '2020-04-22 14:57:38', '2020-04-22 14:57:38'),
(2, 'Sigmund Donnelly I', 'annabel.hirthe@example.com', '2020-04-22 14:57:38', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UQ3QtFk3s1', '2020-04-22 14:57:38', '2020-04-22 14:57:38'),
(3, 'Dr. Miracle Beier', 'dejuan.rodriguez@example.org', '2020-04-22 14:57:38', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Wwv8BEzNb0', '2020-04-22 14:57:38', '2020-04-22 14:57:38'),
(4, 'Mr. Herminio Wyman', 'murphy.jana@example.org', '2020-04-22 14:57:38', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Gobk0o1ZgU', '2020-04-22 14:57:38', '2020-04-22 14:57:38'),
(5, 'Weston Jakubowski', 'cklocko@example.net', '2020-04-22 14:57:38', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bCk9K5jGCj', '2020-04-22 14:57:38', '2020-04-22 14:57:38'),
(6, 'Mrs. Kaelyn Kreiger', 'lucy.brakus@example.net', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'CfjipqPH7x', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(7, 'Donald Hayes IV', 'gnikolaus@example.net', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'tyV8pG7kVg', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(8, 'Monserrat Lehner', 'kassulke.harrison@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YKadYhCk2O', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(9, 'Landen O\'Keefe', 'mclaughlin.kailyn@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'IeKjPzJw0E', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(10, 'Kiara Funk', 'earmstrong@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3WxtHG5VoB', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(11, 'Marcelle White', 'maxime.goodwin@example.net', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6CkyplPQWZ', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(12, 'Alexanne Larkin', 'aaliyah87@example.org', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Qspkgw880r', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(13, 'Garrison Howell V', 'krutherford@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'A2xOmuPKZ0', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(14, 'Fae Erdman IV', 'yvette.toy@example.org', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4ADirdB9as', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(15, 'Dr. Brian Bergstrom DVM', 'shemar.leannon@example.net', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Iu52GPNFWr', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(16, 'Narciso Towne', 'barton.mikayla@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'AnOlhaGXxl', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(17, 'Deven Oberbrunner', 'mabelle23@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ylx8uJakex', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(18, 'Brando Brakus', 'hdaniel@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '9rriNUx9Lg', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(19, 'Vicente McLaughlin', 'pollich.dock@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Aqqlt0VGQ1', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(20, 'Prof. Raymundo Bogisich Sr.', 'awaters@example.net', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2I4k5qnQJ0', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(21, 'Mr. Kenyon Bednar MD', 'eichmann.joseph@example.org', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '22lq6FJtKc', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(22, 'Shaina Anderson', 'morgan.prohaska@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'gr4CGrKHi8', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(23, 'Fredy Schneider MD', 'hdicki@example.org', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'JWPUtazXPt', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(24, 'Mr. Blake Altenwerth DVM', 'jordane.schimmel@example.org', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'jtdm3n1v84', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(25, 'Prof. Emma Ortiz', 'vance.balistreri@example.net', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'MnbA33unio', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(26, 'Rosario Schoen MD', 'nakia.lynch@example.org', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'NzjQxhVny8', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(27, 'Dedric Howell', 'kbailey@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'xBLth584A3', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(28, 'Aurore Greenfelder', 'keebler.bella@example.net', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'jOjIwYY08e', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(29, 'Alexandrine Padberg', 'concepcion42@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'R5NSt1RqfL', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(30, 'Alexanne Dare', 'htromp@example.com', '2020-04-22 14:57:46', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UrjKinoY4C', '2020-04-22 14:57:46', '2020-04-22 14:57:46'),
(31, 'Jarret Watsica Sr.', 'wiza.tyson@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4v5WfKOlMZ', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(32, 'Leon Lebsack', 'schmeler.charlotte@example.org', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dfsCBLXCWO', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(33, 'Armani Kohler', 'dubuque.raphael@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EQqBznqPEH', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(34, 'Jacynthe Bogan', 'napoleon34@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '60muo1gBtl', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(35, 'Gabriella Morar', 'zgraham@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Cc5fXKSpn9', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(36, 'Jacques Russel', 'spencer.karlee@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ND1DHsTd8g', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(37, 'Dr. Raven Lueilwitz', 'dariana.wilderman@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Hp4gO8YhEx', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(38, 'Sammie Lubowitz', 'kunze.gunner@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'TNwujwOQFK', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(39, 'Freeman Stiedemann PhD', 'aisha.beatty@example.net', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1gYbuFwtkT', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(40, 'Ramon Brown', 'fwillms@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'G9OnvLsi2Z', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(41, 'Annabell Bogisich', 'nkuhlman@example.org', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'I3YKJ82MHX', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(42, 'Dr. Justice Koelpin V', 'mazie.goodwin@example.org', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'R8JcaSDbHO', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(43, 'Mr. Lester Raynor', 'clementine.glover@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'KChlsuQXm6', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(44, 'Amara Macejkovic', 'cindy36@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0vhQuz1IVF', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(45, 'Hope McLaughlin V', 'kcartwright@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'V8lzXxxTEr', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(46, 'Ansel Graham', 'sharber@example.net', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'lFbD31u969', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(47, 'Mrs. Orie Cormier Jr.', 'jaylon59@example.org', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dihA1TMixp', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(48, 'Melody Runolfsson', 'twuckert@example.org', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'LRKBubaCrm', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(49, 'Mariane Fisher', 'ogottlieb@example.net', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'MCB8s4gbnN', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(50, 'Chaya Reinger', 'maryam.metz@example.net', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'rGkpiMZjPl', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(51, 'Eleazar Morissette Jr.', 'brielle20@example.org', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ZTXMnE0jo9', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(52, 'Earnest Gulgowski', 'hwisozk@example.net', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0csSM120hR', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(53, 'Claudine Powlowski III', 'romaguera.golden@example.com', '2020-04-22 14:57:57', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dNyYY438FR', '2020-04-22 14:57:57', '2020-04-22 14:57:57'),
(54, 'Gail Quitzon', 'okeefe.sophia@example.org', '2020-04-22 14:57:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'CzJRRv71oz', '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(55, 'Elsa Waelchi DDS', 'uhaley@example.org', '2020-04-22 14:57:58', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Qyt4aTUhMm', '2020-04-22 14:57:58', '2020-04-22 14:57:58'),
(56, 'Anastasia Reichert', 'ischinner@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'KZlN2co9p5', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(57, 'Lamar Wehner', 'barton.kailee@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zSYkerId44', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(58, 'Maximillia Huel', 'ayla38@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'NRaLQjjELx', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(59, 'Lupe Douglas Jr.', 'kessler.joanie@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'w79ee0jtui', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(60, 'Amely Prosacco DVM', 'jaskolski.marietta@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Ab256y8fec', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(61, 'Prof. Kaia Brown', 'bertrand13@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3BAPH4vDjs', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(62, 'Mr. Gay Kulas', 'timmothy.blick@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '8a1gdmKNxA', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(63, 'Mable Kihn', 'kiehn.haleigh@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'z81HmBhjYP', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(64, 'Abelardo Carroll', 'egerlach@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'BXvpquPUza', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(65, 'Mrs. Mabel Konopelski', 'bruen.virgie@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'mEsdbZiJN2', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(66, 'Prof. Zackery Howe', 'isabel.marks@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'tr5NZ9ZXQp', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(67, 'Christopher D\'Amore', 'nnikolaus@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'WHsOcDXaQz', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(68, 'Prof. Nicklaus Mosciski', 'wayne.johnson@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'NDpnLwP1O5', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(69, 'Prof. Johnson Breitenberg V', 'kemmer.kadin@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'xdEwl8X6vr', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(70, 'Mara Lubowitz', 'neoma.volkman@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Xj9D4NYxwl', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(71, 'Miss Brandyn Hagenes', 'johns.princess@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SwqLfJK3mA', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(72, 'Dr. Pinkie Hermiston DDS', 'dedrick.cruickshank@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'slggKh4GEK', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(73, 'Carolyne Ward', 'arnaldo76@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '91Yy4KpOiX', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(74, 'Mr. Luis Lowe V', 'graham.makenzie@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6C2Xq32ZaP', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(75, 'Rodolfo Beer', 'mayert.alberta@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'PpLF3TLky0', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(76, 'Alize Gleichner', 'sgreenfelder@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'TGZcXEITmR', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(77, 'Herminia Windler PhD', 'beahan.mario@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'M0mIne2wiT', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(78, 'Prof. Dustin Wehner', 'chauck@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kZk8FHWXtr', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(79, 'Barbara Blanda', 'lind.brady@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SbzDdlGAC7', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(80, 'Dr. Jaquan Cormier', 'ekerluke@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'nTMGWEdUZo', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(81, 'Ms. Nya Kuhn', 'foconner@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Y984GAxOEM', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(82, 'Jovanny Rippin', 'clebsack@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '59RxWscHaM', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(83, 'Ivy Grant', 'amya05@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'NsD1sMoNsx', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(84, 'Craig Smith', 'zortiz@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'fSsC9HjVT9', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(85, 'Kirstin Raynor', 'cameron83@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'DuKTpr16UB', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(86, 'Ms. Bianka Wunsch DDS', 'predovic.laverna@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'f80qADRBsV', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(87, 'Elwyn Bailey', 'bmosciski@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'mc3w1bLa4H', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(88, 'Ms. Susana Gorczany DDS', 'ireilly@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'uEJeNhou2i', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(89, 'Ms. Aurelia Kris Jr.', 'abdiel12@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'd6pu1Mma9t', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(90, 'Candido Prosacco', 'weimann.bernice@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EXOH6gX8Tw', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(91, 'Micheal Lebsack', 'dboehm@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3LvNI5JWpW', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(92, 'Prof. Eldridge Mann', 'eleanore.hayes@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dE91abhr4t', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(93, 'Hailee Schuster II', 'corrine08@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'HHO8RmO2S2', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(94, 'Sydni Collins Jr.', 'umurazik@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'MwfA2dFUmN', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(95, 'Dr. Filiberto Wiegand', 'dbotsford@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'xhBvx0wL9Q', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(96, 'Katelin King', 'schaden.hassie@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zNypMk9DWy', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(97, 'Issac Hamill', 'esauer@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Tjs7FpogCH', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(98, 'Prof. Harmony Little I', 'boehm.mariam@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'AhfaUgggu3', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(99, 'Cyril Heathcote', 'hermann07@example.org', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Pk5f5Tw9go', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(100, 'Brown Halvorson', 'dooley.lonie@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'jbG89rf1dl', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(101, 'Issac Kemmer', 'clifford87@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '5Tf1ZQTWRF', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(102, 'Cassandre Mayert', 'nakia.watsica@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'PmdQwMHskR', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(103, 'Mr. Devonte Simonis Jr.', 'abel86@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'PWvEjTj0E6', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(104, 'Anthony Luettgen', 'salvador27@example.net', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7vpPsF2puu', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(105, 'Kailey Howe', 'kamille.parker@example.com', '2020-04-22 14:58:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YW08to2ko8', '2020-04-22 14:58:12', '2020-04-22 14:58:12'),
(106, 'Mike Thornley', 'mathornley@gmail.com', NULL, '$2y$10$d0IakJYDvIhGOxaEjhmGm.JyCvHFe3vmt4KrnmR3osZdYOZHJBvP2', NULL, '2020-04-22 15:10:06', '2020-04-22 15:10:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abilities`
--
ALTER TABLE `abilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ability_role`
--
ALTER TABLE `ability_role`
  ADD PRIMARY KEY (`ability_id`,`role_id`),
  ADD KEY `ability_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `address_name`
--
ALTER TABLE `address_name`
  ADD PRIMARY KEY (`name_id`,`address_id`),
  ADD KEY `address_name_address_id_foreign` (`address_id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_tag`
--
ALTER TABLE `article_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `article_tag_article_id_tag_id_unique` (`article_id`,`tag_id`),
  ADD KEY `article_tag_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `names`
--
ALTER TABLE `names`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `responses`
--
ALTER TABLE `responses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abilities`
--
ALTER TABLE `abilities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `article_tag`
--
ALTER TABLE `article_tag`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `names`
--
ALTER TABLE `names`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `responses`
--
ALTER TABLE `responses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ability_role`
--
ALTER TABLE `ability_role`
  ADD CONSTRAINT `ability_role_ability_id_foreign` FOREIGN KEY (`ability_id`) REFERENCES `abilities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ability_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `address_name`
--
ALTER TABLE `address_name`
  ADD CONSTRAINT `address_name_address_id_foreign` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `address_name_name_id_foreign` FOREIGN KEY (`name_id`) REFERENCES `names` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `article_tag`
--
ALTER TABLE `article_tag`
  ADD CONSTRAINT `article_tag_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `article_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
