<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
   	protected $guarded = [];

    public function name()
    {
    	return $this->belongsToMany(Name::class);
    }
}
