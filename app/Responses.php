<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responses extends Model
{

	/**
	 * Get the user that wrote the current instance of the response.
	 * @return [type] [description]
	 */
    public function user()
    {
    	return $this->belongsTo(\App\User::class);
    }

    /**
	 * Get the conversatgion belonging to this reponse instance.
	 * @return [type] [description]
	 */
    public function conversation()
    {
    	return $this->belongsTo(\App\Conversation::class, 'conversation_id');
    }

    /**
     * See if the best best_reply_id from the coversation is the same as the current instance of the response. 
     * @return boolean [description]
     */
    public function isBtestReply()
    {
        //ddd($this->conversation);
        return $this->id === $this->conversation->best_reply_id;
    }
}
