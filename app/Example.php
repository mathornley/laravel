<?php

namespace App;

class Example
{
	/*protected $collaberator;

	public function __construct(Collaberator $collaberator)
	{
		$this->foo = config('services.foo');
		$this->collaberator = $collaberator;
		//$this->foo = $foo
	}*/

	public function __construct($apikey=null)
	{
		$this->apikey = $apikey;
	}

	public function handle()
	{
		if(isset($this->apikey)) die($this->apikey);
		else return 'There is nothing';
	}
}