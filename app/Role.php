<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

	// Allow the mass assignments on the model. 
	protected $guarded = [];

	/**
	 * Get all the abilities for the role instance. 
	 * @return [type] [description]
	 */
    public function abilities()
    {
    	return $this->belongsToMany(Ability::class)->withTimestamps();
    }

    public function allowTo($ability)
    {
    	 // If an ability instance isn't returned and this is a string, look uo the ability and then assign it.
    	if(is_string($ability))
    	{
    		$ability = Ability::whereName($ability)->get();	
    	}

    	return $this->abilities()->sync($ability, false);
    }

}
