<?php

namespace App;

class Example2
{
	public function __construct($string)
	{
		$this->string = 'This is a tester for Mike';
   		//$this->string = config('services.foo');   
	}

	public function handle()
	{
		return $this->string;
	}
}