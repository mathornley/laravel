<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Example;
use Illuminate\Filesystem\Filesystem; // You need this for automatic resolution. 
use Illuminate\Support\Facades\Cache; // You need this for automatic resolution. or use gloabl \Cache

class PagesController extends Controller
{

    public function __construct()
    {

      // Best practice says defines these instances here so you've not got code all over the place.
      $this->file = new Filesystem;
      $this->cache = new Cache;
      //ddd($this->cache);

    }

   /**
    * Resolve the service out of the container and also use automatic resolution
    *
    * Options:
    *
    * 1 - Check the service container and if in there, use: $example = resolve('check'); otherwise, error
    * 2 - Ask for it in the function immediately: function home(\App\Example $example)
    * 3 - Check service container and then if not in there, check classes: $example = app()->make(\App\Example::class);
    * 

    * @return [type] [description]
    */
   public function home(/*Cache $cache*/ Filesystem $file)
   {

      // Set something in the cache for 60 seconds.
      $this->cache::remember('foo', 60, fn()=>'foobar');
      //return $this->cache::get('foo');
      //ddd($cache);

      // *** READING FILES *** Without including, use global alias: \File
      //return $this->file->get(public_path('index.php')); // Using automatic resolution
      //return \File::get(public_path('index.php')); 

   		//$example = resolve('example'); // This is in the service container
      //ddd($example);

   		// Or 
   		//$example = app()->make(\App\Example::class);
   		//$example = resolve(\App\Example::class);
   		//ddd(resolve(\App\Example::class),resolve(\App\Example::class)); // Dump, die and debug (singletons and single instances)

      return view('test');

   }
}
