<?php

namespace App\Http\Controllers;

// To use email Facade
use Illuminate\Support\Facades\Mail;
// To use request Facade.
use Illuminate\Http\Request;

// I've had to explicity add the class otherwise html email Mail::to didn't work
use App\Mail\MailMe; // This is used for the HTML emails
use App\Mail\Contact; // This is used for the markdown emails

class EmailController extends Controller
{
    /**
     * Call the email form
     * @return [type] [description]
     */
    public function index()
    {
    	return view('email_form');
    }

    /**
     * Submit the email
     * @return [type] [description]
     */
    public function store()
    {
    	//dd(config('mail.from')); // Checking the config was set to 'Log'

    	// Valikdate the email address to required and an 'email'
    	request()->validate(['email'=> 'required | email']);

        // *********************************** Standard email
    	/*Mail::raw('It works', function($message){
    		$message->to(request('email'))
    		//->from('inthecontroller@gmail.com', 'Mike Thornley') // can use this or defaults to wherever is in config/mail config
    		->subject('Hello there....test');
    	});*/

        // Just messing - pull something from the database and add that to the HTML template
        $result = \App\Article::find(1);
        $title = $result['title'];

        // *********************************** HTML email
        Mail::to([request('email'), 'mike@mikedeveloper.com']) // Sneding to more than one receipient here.
            ->send(new MailMe('Pants', 'Michael', $title));

    	// redirect and show a message to the user - this is flashed to the session for one message.
    	return redirect('/email/')
    	->with('message', 'Thanks, your email was sent'); // Message for one request only.
    }
}
