<?php

namespace App\Http\Controllers;
use \App\Conversation;

use Illuminate\Http\Request;

class ConversationsController extends Controller
{
	/**
	 * Show all the conversation
	 * @return [type] [description]
	 */
    public function index()
    {
    	// Get everything and send this to the conversation.index view
    	return view('conversation.index', ['conversation'=> 
    		Conversation::all()
    	]);
    }

    /**
     * Show the selected conversation 
     * USE route/model binding (Don't forget to include the class at the top)
     * @param  Conversation $conversation [description]
     * @return [type]                     [description]
     */
    public function show(Conversation $conversation)
    {
        //$this->authorize('view', $conversation); // You could do this to only show to the author. 
        
        // Using route/model binding to fetch the instance
    	return view('conversation.show', compact('conversation'));
    }
}
