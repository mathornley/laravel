<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

// PHP Import the models to the namespace
use App\Article;
use App\Tag;

class ArticlesController extends Controller
{
    /**
     * Get all the articles in the database
     * @return [obj] [This is all the results as an array object]
     */
    public function index()
    {
        // If a a tag param is present in the request, search for articles with that tag.
        if(request('tag'))
        {
            /**
             * 1 - search Tags for the tag by name = gets the instance
             * 2 - Use that instance to then get the articles: $tag->articles;
             * @var [type]
             */
            $articles = Tag::where('name',request('tag'))->firstOrFail()->articles;
            $articles->message = request('tag');
        }

        // No params in the request, so get all the articles.
        else
        {
            $articles = Article::latest()->get();
            //$articles->message = NULL;
        }

        // Finally, return all the articles. 
        return view('articles.index',
            [
                'articles' => $articles
            ]
        );
    }

	/**
	 * Just get the one article in particular from the ID.
	 * @param  [string] $id [This is the ID number of the article]
	 * @return [obj] [This is the required result as an array object]
	 */
    public function show(Article $article) // Using route model binding
    {
    	//dd($id);
    	return view('articles.show',
    		[
    			//'article' => Article::find($id)  // Use thiks if not using route/model binding
                'article' => $article
    		]
    	);
    }

    /**
     * Show a view with a form so we can add a new article, with tags. 
     * @return [HTML] Returns a create view with a form.
     */
    public function create()
    {
        // Get all the tags from the tags table.
        $tags = \App\Tag::all();
        
        // Use compact and return all the tags.
        return view('articles.create',[
            'tags'=>$tags
        ]);
    }

    /**
     * Store the form input for the article to be created.
     * Various stages see code condensced down to make it more readable. 
     * @return redirect to all the articles overview.
     * Mass assignment turned off in the Model: Article
     */
    public function store()
    {
        //dd(request()->all()); // Let's see what has been posted and look in request obj
        
        // Vaidate the form input and save the object
        /*$validated = request()->validate([
            'title'=>['required','min:10','max:255'],
            'text'=>'required',
            'excerpt'=>'required'
        ]);*/

        //dd($validated); // Validated attributes is a copy of what you're submitting when they have passed the validation.
    
        // Add the posted data to the database.
        /*$article = new Article; // Instantiate new instance and add data to the Articles table
        $article->title = request('title');
        $article->text = request('text');
        $article->excerpt = request('excerpt');
        $article->save(); // Persist it*/

        // Mass insert and pass the result from: $validated
        /*Article::create([
            'title' => request('title'),
            'text' => request('text'),
            'excerpt' => request('excerpt')
        ]);*/

        // Pass the result of the validation array to the create function
        
        /**
         * This could be condensed down more and you coould just padded the request inside here
         */
        
        // ************************************ DON'T FORGET ******************************************
        //Article::create($validated); // CAN ONLY BE USED ON MASS ASSIGNED MODELS (see model $fillable var)
        // ********************************************************************************************

        $this->validateArticle();

        // Wrap everything inside the one function call.
        $article = new Article(request(['title','text','excerpt']));
        $article->user_id = 2; //auth()->id; // Set to the id of the current user
        $article->save();

        // Attach the tags from the posted form. If there are none, nothinhg happens, so that's ok.  
        $article->tag()->attach(request('tags'));

        // Redirect to the articles overview page, where the new article will be the newest post.
        //return redirect(route('articles.store'));
        return redirect(route('articles.show', $article)); // Redirect to the updated article. 
    }

    /**
     * Show the form to edit the article and pull in the article from the database using the id.
     * @param  [INT] $id This is the article id from the route.
     * @return [HTML] This is the HTML edit form we'll use to update the record.
     */
    public function edit(Article $article) // Using route model binding
    {

        $this->authorize('update-article', $article); // Only the author can update this. This could also be added to the middleware above.

        // Find the record by the id.
        //$article = Article::find($id);  // Use thiks if not using route/model binding

        //return view('articles.edit',['article'=>Article::find($id)]);

        //dd(Article::find($id)); // Get the article from the id passed in.
        return view('articles.edit',compact('article'));
    }

    /**
     * Update the record and commit to the database.
     * Uses Update method of the parent model: Model
     * @param  [INT] $id This is the id of the article to update.
     * @return [HTML] This is the HTML edit form we'll use to update the record.
     */
    public function update(Article $article) // Using route model binding
    {
         $validate = request()->validate([
            'title'=>['required','max:255'],
            'text'=>'required',
            'excerpt'=>'required' 
        ]);

        // Get the article from the id passed in.
        //$article = Article::find($id); // Use thiks if not using route/model binding

        // Update the article with the posted data to the database.
        /*$article->title = request('title');
        $article->text = request('text');
        $article->excerpt = request('excerpt');
        $article->save(); // Persist it*/

        $article->update($validate);

        // Redirect to the articles overview page, where the new article will be the newest post.
        //return redirect(route('articles.show',$article)); // You can use the name of the route or below and use the call to Article->path() directly on the model. 
        return redirect($article->path());
    }

    /**
     * Validate the form fields submitted and return the array.
     * @return [array] This is an array of the validated form fields or false
     */
    protected function validateArticle()
    {
        return request()->validate([
            'title'=>['required','max:255'],
            'text'=>'required',
            'excerpt'=>'required',
            'tags'=>'exists:tags,id' 
        ]);
    }
}
