<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Article;

class APIController extends Controller
{

	public function __construct()
	{
		$this->article = new Article;
	}

    public function index()
    {
    	$articles = $this->article::all();
		$articles = $articles->last();
		$articles->api = 'This would be the API pulled connection and returned info';
		$articles->test = 'somethingelse';

		$articles->tags;

    	return view('about',compact('articles'));
	}
}
