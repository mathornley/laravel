<?php

namespace App\Http\Controllers;

//use App\MikeFacade;

// *** ALWAYS Use the Fascades namespace ***
use Illuminate\Support\Facades\View; // uses class View, extends Facade
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\File;

// We need this for our example custom Facade (housed with the other controllers)
use \App\MikeFacade;
use \App\MikeFascade;
use \App\AnnFascade;

class FascadesController extends Controller
{
    public function index()
    {
        return AnnFascade::name(); // Mike->handle() method call

    	/**
    	 * Translates to:
    	 * 1 - ExampleFacade::handle(); is called
    	 * 1 - ExampleFacade - getFacadeAccessor() method - returns 'example'
    	 * 2 - appservice provider is checked for 'example' and its bound to Example
    	 * 3 - on Example class, statically call: handle() method
    	 */

    	//return File::get(public_path('index.php'));
    	//return Request::input('name');
    	//return View::make('welcome');
    }
}
