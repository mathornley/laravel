<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use \App\Responses;
use Illuminate\Support\Facades\Gate;

/**
 * 2 ways this can be done:
 * 1. with a polkicy - ConversationPolicy controller.
 * 2. Using the auth service provider, which requires the (Gate facade).
 */
class ConversationsBestReplyController extends Controller
{
	
    /**
     * A way of ensuring the best reply is a legitimate signed-in and owned user.

     * This whole process uses the Gate facade.
     * @param  Responses $responses [description]
     * @return [type]               [description]
     */
    public function store(Responses $responses) // Let's do some route model binding and get the reponse instance
    {
		/**	
    	 * We need to make sure the current user is allowed to 'best-reply' the conversation and that they are authorised to do so.
         * Under the bonnet this uses the Gate fascade 

         * and you can instead, use Gate::allows('update-conversation',$responses->conversation) or Gate:denies('update-conversation',$responses->conversation)
    	 */
        
        // If current user is not authorised. Check the Policy or AuthServiceProvider using Gate() inside boot method of thr class.
        if(Gate::denies('update-conversation',$responses->conversation)) // Would need to include the facade Gate but we're now using the ConversationPolicy we created and the update method of it.
        {
            dd('Authorisation wasn\'t granted');
        }

        // This uses the ConversationPolicy@update to define who is authorised.
    	$this->authorize("update-conversation",$responses->conversation); // ** This is now defined in Policy/ConversationPolicy but under the hood uses the Gata facade **

    	// Get the reponse id as the best_reply_id added to the conversation table.
    	$responses->conversation->setBestReply($responses);
    	
    	// Direct back to the show blade. 
    	return back();
    }
}
