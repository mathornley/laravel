<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

// Allow notifications to be used.
use App\Notifications\PaymentReceived;

// Allows the event to be used. 
use App\Events\ProductPurchase;
use App\Events\MikeTestEvent;


class PaymentsController extends Controller
{
	/**
	 * Get the template to show the initial payment button.
	 * @return [type] [description]
	 */
    public function index()
    {
    	return view('auth.create');
    }

    /**
     * Process the payment once the button in the view has been clicked. 
     * @return [type] [description]
     */
    public function store()
    {

        MikeTestEvent::dispatch('This is Mike');

    	// This is deemed more reabable. Both do exactly the same thing though.
    	// This is better if you're notifying for one user.
        
        // ** EVENT************************* ** for product purchase - 1. Award coupon and unlock the bonus
        
        // ALL event listeners can be checked with: php artisan events:list 
        //ProductPurchase::dispatch('toy'); //or use event(new ProductPurchase('toy'));
        // Events are all connected to listeners in; Illuminate\Foundation\Support\Providers\EventServiceProvider


         // ** NOTIFICATIONS ************************* ** fo

        // Notifications - Notify the user
    	//request()->user()->notify(new PaymentReceived(900,'test')); // This just dummy data but no fascade needed.

    	// or use the notification facade as below

    	//return request()->user()->name; Outputs the signed in users name 'Mike Thornley' from the object
    	//Notification::send(request()->user(), new PaymentReceived(900,'test'));
    }
}
