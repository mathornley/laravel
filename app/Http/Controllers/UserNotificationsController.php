<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserNotificationsController extends Controller
{
	/**
	 * Pull notifications for the current signed in user.
	 * Show the view, with the user notifications being sent.
	 * @return [object] Return the view with the user notifications. 
	 */
   public function index()
   {

      //ddd(request()->user()->notifications);

      // Pull all UNREAD notifications.
      $notifications['unread'] = request()->user()->unreadNotifications;
      
      // Pull all READ notifications.
      $notifications['read'] = request()->user()->readNotifications;

      // Now mark READ notifications as READ - next pass these will disappear.
      $notifications['unread']->markAsRead();

      // Or if you were just returning read/unread solely you could use *** HIGEHER ORDER TAP *** tap()
      //tap(auth()->user()->readNotifications);

      // Return the notifications and the template
		return view('notifications.show', [
			'notifications'=>$notifications
		]);

   }
}
