<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB; // Import DB class from the global namespace.

use App\Posts; // we're using eloquemnt now, so now have a model Posts and include it.

class PostsController extends Controller
{
    public function show($slug)
    {
       // Use the slug and query the table. This is the database call to the table using the slug in WHERE and query builder.
	   //$post = DB::table('posts')->where('slug',$slug)->first();
	   
	   // Using eloquant, we can query the table. 
	   //$post = Posts::where('slug',$slug)->firstOrFail(); // firstOrFail handles the post not being found.

	   // First or fails makes this redundant.
	   /*if(!$post)
	   {
	   		abort(404);
	   }*/
	   
	   // Dump and die the output to test.
	   //dd($post);

	   // Return the data to the view. Make the eloquent query inlined. 
	   return view('posts',['post'=>Posts::where('slug',$slug)->firstOrFail()]);
    }
}
