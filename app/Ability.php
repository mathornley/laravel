<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ability extends Model
{
	// Allow the mass assignments on the model. 
	protected $guarded = [];

    public function roles()
    {
    	return $this->belongsToMany(Role::class)->withTimestamps();
    }
}
