<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    public function user()
    {
    	return $this->belongsTo(\App\User::class);
    }

   	public function responses()
    {
        return $this->hasMany(\App\Responses::class, 'conversation_id');
    }

    /**
     * This is updating the conversation with the best reponse id clicked in views.conversation.show
     * @param Responses $responses [description]
     */
    public function setBestReply(Responses $responses)
    {
    	$this->best_reply_id = $responses->id; // Set the response id to the conversation.
    	$this->save(); // Save the change on the conversation. 
    }
}