<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function articles()
    {
        return $this->hasMany(Article::class); // SELECT * FROM Articles where user_id = 1
    }

    public function projects()
    {
        return $this->hasMany(Posts::class); // SELECT * FROM posts where user_user = 1
    }

    public function conversations()
    {
        return $this->hasMany(Conversation::class); // SELECT * FROM Conversations where user_user = 1
    }

     /**
     * Route notifications for the Nexmo channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForNexmo($notification)
    {
        return '+447912762045';
    }

    /**
     * Test if the current user instance is admin. 
     * @return boolean [description]
     */
    public function isAdmin()
    {
        //ddd('Works!');
        //return $this->admin === 1
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    /** 
     * Assign a role to the user
     * @param  [type] $role [description]
     * @return [type]       [description]
     */
    public function assignRole($role)
    {
        // If a role instance isn't returned and this is a string, look uo the role and then assign it.
        if(is_string($role))
        {
            return Role::whereName($role)->firstOrFail();
        }

        $this->roles()->sync($role, false);
    }

    public function abilities()
    {
        return $this->roles->map->abilities->flatten()->pluck('name')->unique();
    }
}

//$user = User::find(1);
//$projects = $user->projects;
