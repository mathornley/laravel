<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

   protected $guarded = [];

   public function user()
   {
   		return $this->belongsTo(\App\User::class); // One to many, many articles - ONE user. 
   }
   
   public function tag()
   {
   		return $this->belongsToMany(\App\Tag::class);
   }

   /**
    * If a path instance calls this method. 
    * @return [type] [description]
    */
   public function path()
   {
   		return route('articles.show', $this); // returns the path as a string
   }

   /**
    * A quick short cut i set up to edit an article using the named route.
    * This function returns the path and '/articles/id/edit/' 
    * @return [type] [description]
    */
   public function edit()
   {
   		return route('articles.edit', $this); // returns the path as a string
   }
}
