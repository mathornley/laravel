<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

// You need this for the Nexo messaging to work. 
use Illuminate\Notifications\Messages\NexmoMessage;


class PaymentReceived extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($amount,$string)
    {
       $this->amount = $amount;
       $this->test = $string;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // Mail is the default only. Database was added
        return ['mail','database', 'nexmo'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Payment for £'.$this->amount.' by '.request()->user()->name. ' has been sent.')
                    ->greeting('Hello there')
                    ->line('The introduction to the notification.')
                    ->action('Contact Mike', url('/payments'))
                    ->line('Hoping we hear from you soon and have a great day')
                    ->line('The type was '. $this->test);
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmo($notifiable)
    {
        return (new NexmoMessage())
                    ->from('MIKE THORNLEY')
                    ->content('Your message from Mike @ Laravel TEST')
                    ->unicode();
    }

    /**
     * Get the array representation of the notification.
     * This is what is stored in the table as a JSON object, referenced using data['key']
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'amount'=>$this->amount,
            'type'=>$this->test
        ];
    }
}
