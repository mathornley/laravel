<?php

namespace App\Listeners;

use App\Events\ProductPurchase;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RewardMember
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    
    public $test; // Always make these public variables.

    public function __construct()
    {
        $this->test = 'Set this in the RewardMember class';
    }

    /**
     * Handle the event on ProductPurchase.
     *
     * @param  ProductPurchase  $event
     * @return void
     */
    public function handle(ProductPurchase $event)
    {
        var_dump('<p>This member would be rewarded '.$this->test. '.</p>');
    }
}
