<?php

namespace App\Listeners;

use App\Events\MikeTestEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AnotherTest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MikeTestEvent  $event
     * @return void
     */
    public function handle(MikeTestEvent $event)
    {
        var_dump("Here is another test");
    }
}
