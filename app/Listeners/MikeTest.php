<?php

namespace App\Listeners;

use App\Events\MikeTestEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MikeTest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    
    public $test;

    public function __construct()
    {
      $this->test = 'This is another var set';
    }

    /**
     * Handle the event.
     *
     * @param  MikeTestEvent  $event
     * @return void
     */
    public function handle(MikeTestEvent $event)
    {
        //ddd($event);
        var_dump('I am seal, hear me sing!');
    }
}
