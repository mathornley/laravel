<?php

namespace App\Listeners;

use App\Events\ProductPurchase;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AwardCoupon
{
    /**
     * Create the event listener.
     *
     * @return void
     */
     
    public $test;   // Always make these public variables.

    public function __construct()
    {
        $this->test = 'Set this in the AwardCoupon class';
    }

    /**
     * Handle the event on ProductPurchase.
     *
     * @param  ProductPurchase  $event
     * @return void
     */
    public function handle(ProductPurchase $event)
    {
        //dd($event); // $event->name = 'toy';
        var_dump('
            <p>Check the * EVENT SERVER provider to see these wherfe these listeners are declared* </p>'.
            '<p>------------------------------------------------------------------</p>'.
            '<p>The awarded coupon would be given as '. $event->name .' with '. $this->test .'</p>');
    }
}
