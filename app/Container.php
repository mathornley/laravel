<?php

namespace App;

class Container
{
	public $bindings = [];

	public function bind($key, $value)
	{
		$this->bindings[$key] = $value;
	}

	public function resolve($key) // Key is string "Example2"
	{
		if(isset($this->bindings[$key])) return call_user_func($this->bindings[$key]);
		else return false;
	}	
}