<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Name extends Model
{
	protected $guarded = [];

    public function address()
    {
    	return $this->belongsToMany(Address::class);
    }
}
