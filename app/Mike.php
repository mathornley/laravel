<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Mike
{

	public function __construct()
	{
		// Declare you're using the file Facade.
		$this->file = File::class;
	}

    public function go()
    {
    	return 'Yes, I am Mike';
    	//return $this->file::get(public_path('index.php'));
    	//return 'It works';
    }
}
