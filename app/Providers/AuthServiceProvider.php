<?php

namespace App\Providers;

/**
 *  Import our models. 
 */
use App\Conversation;
use App\User;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        // Allow global access to administrators defined here. This is a hook. 
        /*Gate::before(function($user){
            if($user->id==106){
                return true;
            }
        });*/
        

        // For the authorisation on the conversations.show blade so the owner can 'best-reply a response'
        // function(?User $user, Conversation $conversation) would allow ALL guests to see the option
        // $user is a valid authenticated user.
        
        //Gate::define("update-conversation", function(User $user,Conversation $conversation){
       
        // If you have ANY signed-in user that'll do, return true to see the 'best-reply' button.           
        //return true;

        /**
         * You can choose the option -  if the conversation was written by you ONLY!.
         * Get conversation USER, and, if that is the current authenticated user, allow it. 
         */
    
        //return $conversation->user->is($user); // if the user attached to the conv, is this one, that's fine. 
    
        //});
        
        // Global hook and dynamically check the ability for each @can construct
        /*Gate::before(function($user,$ability){
            return $user->abilities()->contains($ability);
        });*/

        // Define each if the user wrote the conversation and can "best-reply" it.
        // Policy is chedked before the service provider
        Gate::define('update-conversation',function($user,$conversation){
            //ddd('Service');
            return $conversation->user->is($user);
        });
        
        // Define each ability
        Gate::define('edit_forum',function($user,$ability){
            return $user->abilities()->contains($ability);
        });

        Gate::define('view_reports',function($user,$ability){
            return $user->abilities()->contains($ability);
        });

        Gate::define('update-article',function($user,$article){
            return $article->user->is($user);
        });

    }
}
