<?php

namespace App\Providers;

use App\Events\ProductPurchase; // You need to include this for the event.
use App\Events\MikeTestEvent; // You need to include this for the event.
use Illuminate\Auth\Events\Registered;

// These must be included otherwise you get a binding resolution error. 
use App\Listeners\AwardCoupon;
use App\Listeners\UnlockBonus;
use App\Listeners\RewardMember;
use App\Listeners\MikeTest;
use App\Listeners\AnotherTest;

use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     * See shouldDiscoverEvents() below for other options on populating the $listen array.
     *
     * @var array
     */
    protected $listen = [

        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        
        // Here are your 3 listeners for a product purchase used in the controller: PaymentsController.php. 
        ProductPurchase::class => [
            AwardCoupon::class,
            UnlockBonus::class,
            RewardMember::class
        ],

        MikeTestEvent::class => [
            MikeTest::class,
            AnotherTest::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    //********************** You don't have to maintain the listeners array above ***********************//

    /**
     * Instead of populating the $listen array above, Laravel can register the listeners here using ** PHPs reflection API ** and then looks for any handle() methods in the listeners directory. 
     * @return [type] [description]
     */
    /*public function shouldDiscoverEvents()
    {
        // This is set in Laravel to default 'FALSE'
        return true;
    }*/
}
