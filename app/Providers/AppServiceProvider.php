<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \App\Collaberator;
use \App\Example;
use \App\Example4;
use \App\Mike;
use \App\Ann;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*app()->bind('example', function(){
            return new \App\Example(new Collaberator); 
        });*/

        // Or due to class instance of $app in ServiceProvider class, same thing
        /*$this->app->bind('example', function(){
            return new \App\Example(new Collaberator); 
        });*/

        // AND remember singletons - same instance on each instantiation
        
        // Or due to class instance of $app in ServiceProvider class, same thing
        // $this->app->singleton('example', function(){ // This would get a single instance only.
        $this->app->bind('example', function(){ 
            $apikey = config('services.foo');
            return new Example($apikey); 
        });

        // Bind to 'example2' to new instance of Exampke2
        /*$this->app->bind('example2', function(){
        return new \App\Example2('This is a string');
        });*/

        $this->app->bind('mike', function(){ 
            //ddd('stop');
            return new Mike(); 
        });

        $this->app->bind('example4', function(){ 
            //ddd('stop');
            return new Example4(); 
        }); 

         $this->app->bind('ann', function(){ 
            //ddd('stop');
            return new Ann(); 
        });    
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
