<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailMe extends Mailable
{
    use Queueable, SerializesModels;

    // NB: All publicly declared vars are available in the view.
    public $string, $name, $title;

    /**
     * Create a new message instances and what data we'll be sending to the view.
     *
     * @return void
     */
    public function __construct(string $string, string $name,string $title)
    {
        $this->string = $string;
        $this->name = $name;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // ************************ Markdown email *****************************************************


        // Get the specific email template for this message.
        /*return $this->markdown('email.contactmemarkdown')
        ->bcc(['test@home.com'])
        ->subject('Mike tester HTML email about ' . $this->string);*/

        // ************************ HTML email *******************************************************

        // Get the specific email template for this message.
        //return $this->markdown('email.contactmemarkdown') // Normal email
        return $this->view('email.contactme') // Normal email
        //->bcc(['test@home.com']) // Can specifiy a to address.
        ->subject($this->name.' tester HTML email about ' . $this->string);
    }
}
