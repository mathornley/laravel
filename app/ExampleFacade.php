<?php

namespace App;

use Illuminate\Support\Facades\Facade;

/**
 * 	@see  App\Example
 */

class ExampleFacade extends Facade 
{
	protected static function getFacadeAccessor()
	{
		return 'example'; // Explicitly bind key to the service container
		// or //
		//return \App\Example::class;
	}
}

// \App\ExampleFacade::handle();

/**
 * The process is as follows:
 * 	1 - You call the class and function: \App\ExampleFacade::handle();
 * 	2 - In ExampleFacade, getFacadeAccessor()     is accessed and the key: 'example is found'
 * 	3 - the container is checked for 'example' and the key and this confirmed as being there
 * 	4 - The key is used in the container and resolves to returning the instantiated class: Example
 * 	5 - On the returned class, the function handle() is called and returns in this case: 'It works'. 
 */