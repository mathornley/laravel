<?php

namespace App\Policies;

use App\Conversation;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * You can these functions anything you want, this is just a standard template. 
 * Just remember if you change the defaults, these must be updated in the templates. 
 */
class ConversationPolicy
{
    use HandlesAuthorization;

    /**
     * Fires before update() method and acts on anything ** NOT NULL ** so don't return false as default!
     * Conversation ** specific policy ** restriction to just: conversations. This is not global.
     * Global access can be granted in the ** AuthServiceProvider ** in the Gate::before() method.
     * Example below: give this admin access to Mike id === 91
     * @param  User   $user [description]
     * @return [type]       [description]
     */
    /*public function before(User $user)
    {
        if($user->id===106){
            return true;
        }
    }*/

    /**
     * Determine whether the user can update the conversation.
     * You could check the table and see if the user is an admin. 
     *
     * @param  \App\User  $user
     * @param  \App\Conversation  $conversation
     * @return mixed
     */
    public function update(User $user, Conversation $conversation)
    {
        //ddd('Policy'); // Policy is checked before the service provider
        //return $user->isAdmin();
        //ddd(User::find($user->id)->email); // You could check the table for an admin == 1 maybe

         //return true; // Would allow ANY signed in used (just checks we have an authenticated $user)
         //return $conversation->user->is($user); // if the user attached to the conv, is this one, that's fine.     
    }

    /**
     * Determine whether the user can view the conversation.
     * The user must be the author of the conversation
     *
     * @param  \App\User  $user
     * @param  \App\Conversation  $conversation
     * @return mixed
     */
    public function view(User $user, Conversation $conversation)
    {
        return true; // Any signed in user
        //return $conversation->user->is($user); // ONLY the author can see
    }

    /**
     * Determine whether the user can view any conversations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }


    /**
     * Determine whether the user can create conversations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can delete the conversation.
     *
     * @param  \App\User  $user
     * @param  \App\Conversation  $conversation
     * @return mixed
     */
    public function delete(User $user, Conversation $conversation)
    {
        //
    }

    /**
     * Determine whether the user can restore the conversation.
     *
     * @param  \App\User  $user
     * @param  \App\Conversation  $conversation
     * @return mixed
     */
    public function restore(User $user, Conversation $conversation)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the conversation.
     *
     * @param  \App\User  $user
     * @param  \App\Conversation  $conversation
     * @return mixed
     */
    public function forceDelete(User $user, Conversation $conversation)
    {
        //
    }
}
