<?php

//Auth::loginUsingId(14); // This is to spoof the user being logged in and used for testing
//auth()->loginUsingId(14); // This is to spoof the user being logged in and used for testing

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ***************************************** Service Containers Demo *************************

/*Route::get('/', function(){

	$container = new \App\Container();

	$container->bind('example', function(){
		return new \App\Example();
	});

	//ddd($container);

	$example = $container->resolve('example');

	ddd($example->go('HERE'));
});*/

/*Route::get('/', function()
{
	$container = new \App\Container;

	$container->bind('example', function(){
		return new \App\Example;
	});

	$example = $container->resolve('example');

	ddd($example->go('HERE'));
});*/

Route::get('/eloquent/', 'TestEloquent@index');

Route::get('/pagestest/', 'MikeTest@home');

/*Route::get('/mikequicktest/', function(){
	return request()->all()['name'];
});*/


// ******* ******************** Custom Service Containers demo *************************


// ** Laravel's container is actually the application itself **

// To access the service container globally use app(), as below:
/*$app->bind('example2', function(){
	return new \App\Example2('This is a string');
});*/

Route::get('/containers', function(){
	
	// Create the custom container: Container This would normally be in app service provider
	$container = new \App\Container();

	// Bind to Container: 'example2', as an instance of Example2
	$container->bind('example2', function(){
		return new \App\Example2('This is a string');
	});

	// Resolve the name from the container = new instance of Example2
	$example =  $container->resolve('example2');

	// Call example 2 method handle()
	ddd($example->handle());

});

// This is not set in the service provider or anywhere else:
Route::get('/unsetbind/', function(/*\App\Example3 $example3*/){
	//ddd(app()->make(\App\Example3::class)); // Same thing, just call the class.
	//resolve('example3'); // Resolve it in the usual way as it's declared in the service provider
	$example3 = resolve(\App\Example3::class); // Same thing, just call the class.
	ddd($example3); // The function call as above, allows you to just ask for it.

});

/**
To summarise:
- bind it to the service container or your own container and resolve it. resolve('example3') 
- resolve a class path resolve(\App\Example3::class)
- Just ask for it in the function call: Route::get('/unsetbind/', function(\App\Example3 $example3*) 
 */

// *************************** EndCustom Service Containers demo *************************



// *************************************** Contai


// ********************************************* Final lesson 50 ****************************************

Route::get('/lesson50', function(){
	//return 'TEST';
	return view('welcome2');
})->middleware('auth'); // Make sure the user is signed in.


 // ** Lockdown the end points - the links won't show in the page but are still otherwise accessible by the URL **

// Make sure the user is allowed to view the reports the forum if they navigate to the link.
Route::get('/view_reports', function(){
	return 'View the reports';
})->middleware('can:view_reports'); 

// Make sure the user is allowed to edit the forum if they navigate to the link.
Route::get('/edit_forum', function(){
	return 'Edit the forum';
})->middleware('can:edit_forum'); 

// ****************************************** End Final lesson 50 ****************************************


// ***********  Auth conversations demo Auth service provider and the conversation policy ************

Route::get('/conversations/', 'ConversationsController@index')->middleware('auth');
Route::get('/conversations/{conversation}/', 'ConversationsController@show');
	//->middleware('can:view,conversation'); // Can only be viewed by the author, conversation instance sent
	//->middleware('auth'); // Must be signed in // See the auth service provider.

// Don't for get the argument name when using route/model binding, it's important.
Route::post('/conversations/{responses}/','ConversationsBestReplyController@store');

// ****************************************End Auth conversations demo *************************


Route::get('/', 'PagesController@home')->middleware('auth'); // And use of singletons

// ********************************************Fascades tester **********************************
Route::get('/fascades', 'FascadesController@index');


// ****************************************Payments demo end *************************

// User notifications
Route::get('/notifications','UserNotificationsController@index')->middleware('auth');

// ****************************************Email demo ends *************************


// ****************************************Payments demo *************************

Route::get('payments/', 'PaymentsController@index')->middleware('auth');
Route::post('payments/', 'PaymentsController@store')->middleware('auth');

// ****************************************Payments demo end *************************

// Show the form
Route::get('/email','EmailController@index');
// Submit the form
Route::post('/email','EmailController@store');

// ****************************************Email demo ends *************************


// Simple test route, welcome page - keep this as the homepage 
/*Route::get('/', function()
{
	return view('welcome');
});*/


Route::get('/clients', function()
{
	return view('clients');
});

Route::get('/about/', 'APIController@index');
	
// Get ALL articles
Route::get('/articles/', 'ArticlesController@index')->name('articles.index');

// Create AN article
Route::get('/articles/create', 'ArticlesController@create')->name('articles.create');

// Edit AN article: show the form
Route::get('/articles/{article}/edit', 'ArticlesController@edit')->name('articles.edit');

// Update and save an article: final save the changes.
Route::put('/articles/{article}', 'ArticlesController@update')->name('articles.update');

// Store an article
Route::post('/articles/', 'ArticlesController@store')->name('articles.store'); 

// Get a particular article
Route::get('/articles/{article}', 'ArticlesController@show')->name('articles.show');

// The contact page.
Route::get('/contact', function()
{
	return view('contact');
});

// ********************************************* THIS IS THE TESTER BIT ***************************************************

/**
 * For example, grab the name from the route. 
 * 2 ways: one with the request() function and one, where it's passed in to the function.
 */
Route::get('/name/{name}',function(){
	$name = request('name'); // Get the name var without it being passed in. 
	// If yhou didn't pass it as an argument, you wouldn't need to get it from request();
	return $name;
});

// Pass in a named parameter to a view - without a controller
Route::get('/name', function()
{
    $name = request('name'); // Get the value from the named parameter
    return view('name',['name'=>$name]); // Pass this param to and fetch the 'name' view
});


// *********************************************** WILDCARDS ******************************


// Wildcard param in route.
/*Route::get('anything/{value}',function(){
	return 'YES';
});*/

// Get the wildcard parameter in the route.
Route::get('something/{value}',function(){
	return request('value');
});

// Get the wildcard parameter in the route.
Route::get('anything/{value}',function($value){
	return $value;
});

// For example, do a db look up. Choose a day of the week in the route as a 'wildcard'.
Route::get('/days/{day}',function($day){
	
	// Days of the week array
	$days = 
	[
		'monday'=>'First',
		'tuesday'=>'Second',
		'wednesday'=>'Third',
		'thursday'=>'fourth',
		'friday'=>'fith',
		'saturday'=>'Sixth',
		'sunday'=>'Seventh'
	];

	// If the correct day isn't used in the URL 
	if(!array_key_exists($day,$days))
	{
		// Use the Laravel function abort
		abort(404, 'Sorry, that day is not found');
	}

	// Fetch the template and send the day chosen to it. 
	return view('day',['day'=>$days[$day]]);
	
});

// **************************************** PASS TO A CONTROLLER *************************************************************


// Using a controller to get a blog post, send the post slug to get the blog post
Route::get('posts/{post}', 'PostsController@show');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
